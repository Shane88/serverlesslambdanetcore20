
# Getting Started with Serverless and .NET Core 2.0

Steps to get up and running with this simple demo

## 1. Install npm

Check that you have node and npm installed
To check if you have Node.js installed, run this command in your terminal:

    node -v

To confirm that you have npm installed you can run this command in your terminal:

    npm -v

If not go to the following link to download and install npm https://www.npmjs.com/get-npm

## 2. Install Serverless Framework

Next the Serverless Framework needs to be installed which is a package for npm.

    npm install -g serverless

## 3. Setup AWS Credentials

The Serverless Framework needs access to your cloud provider's account so that it can create and manage resources on your behalf. It's recommended that you setup an user in your AWS IAM specifically for use with the serverless framework. 

***Do not use your root AWS account with this project***

The permission set that the user will need is shown below. It is recommended to put this into it's own IAM Policy called "ServerlessPolicy".

    {
        "Version": "2012-10-17",
        "Statement": [{
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "states:*",
                "iam:*",
                "apigateway:*",
                "s3:*",
                "logs:*",
                "lambda:*",
                "cloudformation:*"
            ],
            "Resource": "*"
        }]
    }


Once you have a user to use with the Serverless Framework use the following link to learn how to setup your AWS credentials locally https://serverless.com/framework/docs/providers/aws/guide/credentials/

## 5. Build the .NET Core 2.0 project

Using a terminal run

    build.cmd

This will build the .NET project and produce zip file *bin\release\netcoreapp2.0\deploy-package.zip*. This is the file that will be uploaded to the AWS Lambda fuctions in the following steps.

## 6. Deploy the Service

Using a terminal run

    deployall.cmd

The first time this is run it could take take a minute or two. This simply runs the serverless command

```
serverless deploy -v
```

This is the command to tell serverless to deploy everything, including:

1. Cloudformation stacks
2. AWS Lambda Functions
3. S3 Buckets for Logs
4. API Gateway
5. IAM roles for the lambda to execute as

If there are errors during while running this command it may indicate the AWS credentials have not been set up correctly, recheck the "Setup AWS Credentials" of this readme.

## 7. Test the Function

Using a terminal run

```
invokefunction.cmd
```

This will invoke the AWS Lambda function that was deployed and return any logs to the output of the terminal.

## 8. Update the Function 

Inside the *Handler.cs* file, add the following line at the start of the *Hello* function.

```csharp
Console.WriteLine("This is a log from my lambda!");
```

Rebuild the project

```
build.cmd
```

Deploy the function

```
deployfunction.cmd
```

Invoke the function to see the updated code run

```
invokefunction.cmd
```

You will see the new functions output that will include the new Console.WriteLine log.


