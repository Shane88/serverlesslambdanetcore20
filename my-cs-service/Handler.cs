using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using HttpMultipartParser;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

[assembly:LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AwsDotnetCsharp
{
    public class Handler
    {
        private bool _isDebugLogEnabled = false;

        public APIGatewayProxyResponse Hello(ILambdaContext context)
        {   
            // Three different ways to log.
            Console.WriteLine("Console.WriteLine - You will be able to see this log in the Cloudwatch logs.");
            context.Logger.LogLine("context.Logger.LogLine - You will be able to see this in the CloudWatch logs and the" +
                                   "when invoking the function from serverless.");
            LambdaLogger.Log("LambdaLogger.Log - You will see this in the Cloudwatch logs and when invoking the " +
                             "function from serverless." + Environment.NewLine);

            var responseBodyModel = new 
            {
                stringprop = "stringprop",
                intprop = 42,
                dateprop = DateTime.UtcNow,
                nestedobject = new 
                {
                    nestedprop1 = "nestedprop1"
                }
            };

            string bodyString = JsonConvert.SerializeObject(responseBodyModel);

            return new APIGatewayProxyResponse() 
		    {
			    Body = bodyString,
			    StatusCode = 200,
			    IsBase64Encoded = false,
                Headers = new Dictionary<string, string>()
                {
                    { "content-type", "application/json" }   
                }
		    };
        }

        public APIGatewayProxyResponse Echo(APIGatewayProxyRequest request)
        {
            string requestAsString = JsonConvert.SerializeObject(request);

            return new APIGatewayProxyResponse
            {
			    Body = requestAsString,
			    StatusCode = 200,
			    IsBase64Encoded = false,
                Headers = request.Headers
            };
        }

        public APIGatewayProxyResponse JsonifyFormData(APIGatewayProxyRequest request, ILambdaContext context)
        {
            _isDebugLogEnabled = 
                Environment.GetEnvironmentVariable("EnableDebugLog").Equals("true", StringComparison.OrdinalIgnoreCase);

            string formDataAsJson = "{ \"Error\": \"No Form Data Supplied\" }"; 
            int statusCode = 400;

            if (_isDebugLogEnabled)
            {
                string requestAsString = JsonConvert.SerializeObject(request);
                context.Logger.LogLine("Inside JsonifyFormData with request" + requestAsString);
            }

            if (!string.IsNullOrWhiteSpace(request.Body))
            {
                formDataAsJson = ExtractBodyAsJson(request, context);
                statusCode = 200;
            }

            return new APIGatewayProxyResponse
            {
			    Body = formDataAsJson,
			    StatusCode = statusCode,
			    IsBase64Encoded = false,
                Headers = new Dictionary<string, string>()
                {
                    { "content-type", "application/json" }   
                }
            };
        }

        private string ExtractBodyAsJson(APIGatewayProxyRequest request, ILambdaContext context)
        {
            string bodyToParse = request.Body;
            string formDataAsJson = null;
            Dictionary<string, string> formData = null;

            if (request.IsBase64Encoded)
            {
                byte[] data = Convert.FromBase64String(bodyToParse);
                bodyToParse = Encoding.UTF8.GetString(data);

                if (_isDebugLogEnabled)
                {
                    context.Logger.LogLine("Decoded body:" + bodyToParse);
                }
            }
        
            if (request.Headers.ContainsKey("Content-Type") && 
                request.Headers["Content-Type"].Equals("application/x-www-form-urlencoded", StringComparison.OrdinalIgnoreCase))
            {
                // This is just query string format.
                NameValueCollection contents = HttpUtility.ParseQueryString(bodyToParse);

                formData = contents.AllKeys.ToDictionary(k => k, k => contents[k]);
            }
            else
            {
                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(bodyToParse)))
                {
                    MultipartFormDataParser parser = new MultipartFormDataParser(ms);

                    formData = parser.Parameters.ToDictionary(item => item.Name, item => item.Data);
                }    
            }

            if (formData != null)
            {
                formDataAsJson = JsonConvert.SerializeObject(formData);
            }

            return formDataAsJson;
        }
    }
}
